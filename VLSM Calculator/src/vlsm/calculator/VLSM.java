/*
    Calculadora de máscaras de subredes de tamaño variable
    Author: Giovanni Méndez Marín
    Fecha: Domingo 24 de Abril del 2016
 */
package vlsm.calculator;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gmendezm
 */
public class VLSM {

    public static void main(String[] args) {

        try {
            // Escriba aquí la red inicial
            String redInicial = "192.168.1.0/24";
            int cantidadDeHostRequeridos = 4;
            Map<String, Integer> subredes = new HashMap<>(); // [nombre: tamaño]

            subredes.put("Adriana", 42);
            subredes.put("Ericka", 12);
            subredes.put("Gloriana", 51);
            subredes.put("Pamela", 48);

            List<Subred> salida = calcularVLSM(redInicial, subredes);

            for (Subred subred : salida) {

                System.out.println("Nombre de subred: " + subred.nombre);
                System.out.println("Tamaño requerido: " + subred.tamañoRequerido);
                System.out.println("Tamaño asignado: " + subred.tamañoAsignado);
                System.out.println("Dirección: " + subred.dirección);
                System.out.println("Máscara: " + subred.mascara);
                System.out.println("Máscara con formato: " + subred.máscaraCuarteto);
                System.out.println("Rango: " + subred.rango);
                System.out.println("Broadcast: " + subred.broadcast);
                System.out.println("\n");

            }

            imprimirEnPDF(salida);
        } catch (FileNotFoundException ex) {
            System.out.println("No se puede escribir el pdf en esa dirección");
        } catch (DocumentException ex) {
            Logger.getLogger(VLSM.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    static void imprimirEnPDF(List<Subred> salida) throws FileNotFoundException, DocumentException {

        FileOutputStream archivo = new FileOutputStream("Reporte Subredes.pdf");
        Document documento = new Document();
        PdfWriter.getInstance(documento, archivo);
        documento.open();

        documento.add(new Paragraph("Reporte de subredes"));
        documento.add(new Paragraph("I Parcial Redes"));
        documento.add(new Paragraph("\n"));

        for (Subred subred : salida) {

            documento.add(new Paragraph("Nombre de subred: " + subred.nombre));
            documento.add(new Paragraph("Tamaño requerido: " + subred.tamañoRequerido));
            documento.add(new Paragraph("Tamaño asignado: " + subred.tamañoAsignado));
            documento.add(new Paragraph("Dirección: " + subred.dirección));
            documento.add(new Paragraph("Máscara: " + subred.mascara));
            documento.add(new Paragraph("Máscara con formato: " + subred.máscaraCuarteto));
            documento.add(new Paragraph("Rango: " + subred.rango));
            documento.add(new Paragraph("Broadcast: " + subred.broadcast));
            documento.add(new Paragraph("\n"));

        }

        documento.close();
    }

    /**
     * Cálulo de máscaras de subredes de tamaño variable
     *
     * @param redInicial Red Inicial
     * @param subredes Un mapa de las subredes requeridas
     * @return Una lista de las subredes resultantes
     */
    private static List<Subred> calcularVLSM(String redInicial, Map<String, Integer> subredes) {
        Map<String, Integer> subredesOrdenadas = ordenarMapa(subredes);
        List<Subred> salida = new ArrayList<>();
        int ipActual = buscarPrimeraDirecciónIP(redInicial);

        for (String clave : subredesOrdenadas.keySet()) {  // para cada una de las subredes
            Subred subred = new Subred();

            subred.nombre = clave;
            subred.dirección = binarioEnIPString(ipActual);

            int tamañoRequerido = subredesOrdenadas.get(clave);
            subred.tamañoRequerido = tamañoRequerido;

            int máscara = calcularMáscara(tamañoRequerido);
            subred.mascara = "/" + máscara;
            subred.máscaraCuarteto = convertirEnMáscaraCuarteto(máscara);

            int tamañoAsignado = encontrarCantidadUtilizableDeHosts(máscara);
            subred.tamañoAsignado = tamañoAsignado;
            subred.broadcast = binarioEnIPString(ipActual + tamañoAsignado + 1);

            String primerHostUtilizable = binarioEnIPString(ipActual + 1);
            String últimoHostUtilizable = binarioEnIPString(ipActual + tamañoAsignado);
            subred.rango = primerHostUtilizable + " - " + últimoHostUtilizable;

            salida.add(subred);

            ipActual += tamañoAsignado + 2;
        }

        return salida;
    }

    /**
     * Ordenamiento del mapa de forma descendiente.
     *
     * @param mapa Mapa
     * @return Mapa ordenado
     */
    private static Map<String, Integer> ordenarMapa(Map<String, Integer> mapa) {
        List<Map.Entry<String, Integer>> entradas = new ArrayList<>(mapa.entrySet());

        Collections.sort(entradas, new Comparator<Map.Entry<String, Integer>>() {

            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());  // descendiente
            }

        });

        Map<String, Integer> mapaOrdenado = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> entry : entradas) {
            mapaOrdenado.put(entry.getKey(), entry.getValue());
        }

        return mapaOrdenado;
    }

    /**
     * Convierte la dirección IP en un string binario
     *
     * @param direcciónIP Dirección IP
     * @return Un número entero binario que almacena la IP
     */
    private static int direcciónIPEnBinario(String direcciónIP) {
        String[] direcciónIPSeparada = direcciónIP.split("\\.|/");

        int octeto1 = Integer.parseInt(direcciónIPSeparada[0]);
        int octeto2 = Integer.parseInt(direcciónIPSeparada[1]);
        int octeto3 = Integer.parseInt(direcciónIPSeparada[2]);
        int octeto4 = Integer.parseInt(direcciónIPSeparada[3]);

        int salida = octeto1;
        salida = (salida << 8) + octeto2;
        salida = (salida << 8) + octeto3;
        salida = (salida << 8) + octeto4;

        return salida;
    }

    /**
     * Separa el string binario en una dirección IP.
     *
     * @param direcciónIP String binario con la dirección IP
     * @return Dirección IP
     */
    private static String binarioEnIPString(int direcciónIP) {
        int octeto1 = (direcciónIP >> 24) & 255;
        int octeto2 = (direcciónIP >> 16) & 255;
        int octeto3 = (direcciónIP >> 8) & 255;
        int octeto4 = direcciónIP & 255;

        return octeto1 + "." + octeto2 + "." + octeto3 + "." + octeto4;
    }

    /**
     * Encuentra la primera dirección IP para la red especificada.
     *
     * @param redInicial IP de la red inicial
     * @return La primera dirección IP
     */
    private static int buscarPrimeraDirecciónIP(String redInicial) {
        String[] ip = redInicial.split("/");
        int máscara = Integer.parseInt(ip[1]); // parse CIDR mask
        int ajuste = Integer.SIZE - máscara;

        int direcciónPrincipalEnCuarteto = direcciónIPEnBinario(redInicial);
        int primeraDirecciónIP = (direcciónPrincipalEnCuarteto >> ajuste) << ajuste;

        return primeraDirecciónIP;
    }

    /**
     * Calcula la máscara y la retorna en notación CIDR.
     *
     * @param tamañoRequerido El tamaño de la subred
     * @return la máscara
     */
    private static int calcularMáscara(int tamañoRequerido) {
        int bitMásSignificativo = Integer.highestOneBit(tamañoRequerido);
        int posición = (int) (Math.log(bitMásSignificativo) / Math.log(2));
        return Integer.SIZE - (posición + 1);   // +1 ya que la posición inicia en 0
    }

    /**
     * Encuentra el número total de direcciones IP y host utilizables.
     *
     * @param máscara Máscara
     * @return Número total de hosts
     */
    private static int encontrarCantidadUtilizableDeHosts(int máscara) {
        return (int) Math.pow(2, Integer.SIZE - máscara) - 2;
    }

    /**
     * Convierte la máscara en formato de cuarteto.
     *
     * @param mask Máscara en notación CIDR
     * @return Máscara en forma de cuarteto
     */
    private static String convertirEnMáscaraCuarteto(int máscaraCIDR) {
        if (máscaraCIDR == 0) {
            return "0.0.0.0";
        }
        int ajusteCorrimiento = -1;    // '255.255.255.255'
        int conCorrimiento = ajusteCorrimiento << (Integer.SIZE - máscaraCIDR);

        return binarioEnIPString(conCorrimiento);
    }

    private static class Subred {
        public String nombre;
        public int tamañoRequerido;
        public int tamañoAsignado;
        public String dirección;
        public String mascara;
        public String máscaraCuarteto;
        public String rango;
        public String broadcast;
    }
}
